//
//  File.swift
//  Bibliothèque
//
//  Created by Max Mamis on 11/2/15.
//  Copyright © 2015 Prolific Interactive. All rights reserved.
//

import UIKit

extension UIColor {
    
    convenience init(hexString:String) {
        self.init(
            red:   CGFloat( strtoul( String(Array(hexString.characters)[1...2]), nil, 16) ) / 255.0,
            green: CGFloat( strtoul( String(Array(hexString.characters)[3...4]), nil, 16) ) / 255.0,
            blue:  CGFloat( strtoul( String(Array(hexString.characters)[5...6]), nil, 16) ) / 255.0, alpha: 1 )
    }
    
    static func prolificBlue() -> UIColor {
        return UIColor(hexString:"#0093FF")
    }
}