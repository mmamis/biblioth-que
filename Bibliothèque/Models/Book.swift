//
//  Book.swift
//  Bibliothèque
//
//  Created by Max Mamis on 11/2/15.
//  Copyright © 2015 Prolific Interactive. All rights reserved.
//

import Foundation
import Gloss
import Kali

struct Book: Glossy, Equatable {
    
    var author: String?
    var title: String?

    var categories: [String]?
    var publisher: String?
    
    let url: String?

    var lastCheckedOut: DateTime?
    var lastCheckedOutBy: String?

    private let dateStringFormatForEncoding = "yyyy-MM-dd HH:mm:ss zzz"
    private let dateStringFormatForDecoding = "yyyy-MM-dd HH:mm:ss"
    
    init?(json: JSON) {
        guard let author: String = "author" <~~ json,
            let title: String = "title" <~~ json,
            let url: String = "url" <~~ json else {
                return nil
        }
        
        self.author = author
        self.title = title
        self.url = url
        
        if let categoriesString: String = "categories" <~~ json {
            self.categories = categoriesString.componentsSeparatedByString(",")
        } else {
            self.categories = nil
        }
        
        if let dateString: String = "lastCheckedOut" <~~ json {
            self.lastCheckedOut = DateTime.init(string: dateString, format: self.dateStringFormatForDecoding)
        } else {
            self.lastCheckedOut = nil
        }
        
        self.lastCheckedOutBy = "lastCheckedOutBy" <~~ json
        if (self.lastCheckedOutBy == "") {
            self.lastCheckedOutBy = nil
        }
        
        self.publisher = "publisher" <~~ json
    }
    
    init() {
        self.url = nil
    }
    
    func toJSON() -> JSON? {
        var lastCheckedOutByValue: String
        
        if let lastCheckedOutBy = self.lastCheckedOutBy {
            lastCheckedOutByValue = lastCheckedOutBy
        } else {
            lastCheckedOutByValue = ""
        }
        
        return jsonify([
            "author" ~~> self.author,
            "title" ~~> self.title,
            "categories" ~~> self.categories?.joinWithSeparator(","),
            "publisher" ~~> self.publisher,
            "lastCheckedOut" ~~> self.lastCheckedOut?.stringValue(format: self.dateStringFormatForEncoding),
            "lastCheckedOutBy" ~~> lastCheckedOutByValue
        ])
    }
}

func isEqual<T: Equatable>(lhs:T?, rhs:T?) -> Bool {
    
    if lhs == nil && rhs == nil {
        return true
    }
    
    if let rhs = rhs,
        lhs = lhs {
            return rhs == lhs
    } else {
        return false
    }
}

func isEqual<T: Equatable>(lhs:[T]?, rhs:[T]?) -> Bool {
 
    if lhs == nil && rhs == nil {
        return true
    }
    
    if let rhs = rhs,
        lhs = lhs {
        return rhs == lhs
    } else {
        return false
    }
}


func ==(lhs:Book, rhs:Book) -> Bool {
    return isEqual(lhs.author, rhs: rhs.author)
        && isEqual(lhs.title, rhs: rhs.title)
        && isEqual(lhs.lastCheckedOutBy, rhs: rhs.lastCheckedOutBy)
        && isEqual(lhs.lastCheckedOut, rhs: rhs.lastCheckedOut)
        && isEqual(lhs.categories, rhs:rhs.categories)
        && isEqual(lhs.publisher, rhs: rhs.publisher)
        && isEqual(lhs.url, rhs: rhs.url)
}