//
//  BookDetailFormCell.swift
//  Bibliothèque
//
//  Created by Max Mamis on 11/2/15.
//  Copyright © 2015 Prolific Interactive. All rights reserved.
//

import UIKit

protocol BookDetailFormCellDelegate: class {
    func formCellDidUpdateValue(cell: BookDetailFormCell, value: String?)
    func formCellDidTapReturn(cell:BookDetailFormCell)
}

class BookDetailFormCell: UITableViewCell, UITextFieldDelegate {
    
    weak var delegate: BookDetailFormCellDelegate?
    
    @IBOutlet private weak var textField: UITextField!
    
    func layoutWithTitle(title: String, data: String?, returnKeyType: UIReturnKeyType) {
        self.textField.placeholder = title
        self.textField.text = data
        self.textField.delegate = self
        self.textField.returnKeyType = returnKeyType
    }
    
    func beginEditing() {
        self.textField.becomeFirstResponder()
    }

    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        let value = (textField.text as NSString?)?.stringByReplacingCharactersInRange(range, withString: string)
        self.delegate?.formCellDidUpdateValue(self, value: value)
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.delegate?.formCellDidTapReturn(self)
        return true
    }
}
