//
//  BookDetailViewController.swift
//  Bibliothèque
//
//  Created by Max Mamis on 11/2/15.
//  Copyright © 2015 Prolific Interactive. All rights reserved.
//

import UIKit
import Kali

enum BookDetailFormField: Int {
    case Title
    case Author
    case Publisher
    case Categories
    
    static func numberOfValues() -> Int {
        return 4
    }
    
    func title() -> String {
        switch self {
        case .Title:
            return "Title"
        case .Author:
            return "Author"
        case .Publisher:
            return "Publisher"
        case .Categories:
            return "Categories"
        }
    }
}

class BookDetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, BookDetailFormCellDelegate,  BookDetailCheckoutViewDelegate, CheckoutNameManagerDelegate {
    
    var book: Book?
    var editingBook: Book?
    
    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var checkoutView: BookDetailCheckoutView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupData()
        self.updateSaveButtonState()
        self.checkoutView.delegate = self
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.view.layoutIfNeeded()
        self.updateCheckoutViewStateAnimated(false)
    }
    
    // MARK: Actions
    
    func close() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction private func save(sender: AnyObject) {
        self.view.endEditing(true)
        
        guard let book = self.editingBook else {
            return
        }
        
        if self.editingBook?.url == nil {
            self.addNewBook(book)
        } else {
            self.updateBook(book, success: nil)
        }
    }
    
    // MARK: Private Methods - Design
    
    private func setupBarButtonItems() {
        if self.presentingViewController != nil {
            var title: String
            
            if self.book != nil {
                title = "Close"
            } else {
                title = "Cancel"
            }
            
            self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(title: title, style: .Plain, target: self, action: "close")
        }
    }
    
    // MARK: Private Methods - Data
    
    private func setupData() {
        if let book = self.book {
            self.editingBook = book
            self.navigationItem.title = book.title
        } else {
            self.editingBook = Book()
            self.navigationItem.title = "Add Book"
        }
        
        self.setupBarButtonItems()
    }
    
    private func updateCheckoutViewStateAnimated(animated:Bool) {
        if self.book?.url == nil {
            self.checkoutView.setState(.IneligibleForCheckout, animated: animated)
            return
        }
        
        if let checkoutDate = self.book?.lastCheckedOut,
            checkoutName = self.book?.lastCheckedOutBy {
            let checkoutDateString = checkoutDate.stringValue(format: "MMM'.' d")
            self.checkoutView.setState(.CheckedOut("This book was checked out on \(checkoutDateString) by \(checkoutName)"), animated: animated)
        } else {
            self.checkoutView.setState(.Available, animated: animated)
        }
    }
    
    private func stringValueForFormField(field:BookDetailFormField) -> String? {
        switch field {
        case .Title:
            return self.editingBook?.title
        case .Author:
            return self.editingBook?.author
        case .Publisher:
            return self.editingBook?.publisher
        case .Categories:
            return self.editingBook?.categories?.joinWithSeparator(", ")
        }
    }
    
    private func saveValueForCellAtIndexPath(value: String?, indexPath:NSIndexPath) {
        guard let field = BookDetailFormField.init(rawValue: indexPath.row) else {
            return
        }
        
        switch field {
        case .Title:
            self.editingBook?.title = value
            break
        case .Author:
            self.editingBook?.author = value
            break
        case .Publisher:
            self.editingBook?.publisher = value
            break
        case .Categories:
            self.editingBook?.categories = value?.componentsSeparatedByString(", ")
            break
        }
    }
    
    private func updateSaveButtonState() {
        if let author = self.editingBook?.author,
            title = self.editingBook?.title {
            self.navigationItem.rightBarButtonItem?.enabled = (author != "" && title != "")
        } else {
            self.navigationItem.rightBarButtonItem?.enabled = false
        }
        
        // Leave the button disabled if the book hasn't been modified
        if self.editingBook == self.book {
            self.navigationItem.rightBarButtonItem?.enabled = false
        }
    }
    
    // MARK: Private Methods - Networking
    
    private func addNewBook(book: Book) {
        LibraryFacade.sharedFacade.postBook(book) { (savedBook, error) -> Void in
            if let book = savedBook {
                self.book = book
                self.setupData()
                self.updateCheckoutViewStateAnimated(true)
            }
        }
    }
    
    private func updateBook(book: Book, success:((updatedBook: Book) -> Void)?) {
        LibraryFacade.sharedFacade.updateBook(book) { (savedBook, error) -> Void in
            if let book = savedBook {
                self.book = book
                self.setupData()
    
                if let success = success {
                    success(updatedBook: book)
                }
            }
        }
    }
    
    private func updateBookForCheckoutAction(book: Book) {
        self.updateBook(book) { (updatedBook) -> Void in
            self.editingBook?.lastCheckedOut = updatedBook.lastCheckedOut
            self.editingBook?.lastCheckedOutBy = updatedBook.lastCheckedOutBy
            
            self.updateCheckoutViewStateAnimated(true)
        }
    }
    
    // MARK: UITableViewDelegate / UITableViewDataSource

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return BookDetailFormField.numberOfValues()
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("BookDetailFormCell") as! BookDetailFormCell
        let field = BookDetailFormField.init(rawValue: indexPath.row)!
        
        var returnKeyType: UIReturnKeyType
        if indexPath.row == BookDetailFormField.numberOfValues() - 1 {
            returnKeyType = .Go
        } else {
            returnKeyType = .Next
        }
        
        cell.layoutWithTitle(field.title(), data: self.stringValueForFormField(field), returnKeyType:returnKeyType)
        cell.delegate = self
        return cell
    }
    
    // MARK: BookDetailFormCellDelegate
    
    func formCellDidUpdateValue(cell: BookDetailFormCell, value: String?) {
        guard let indexPath = self.tableView.indexPathForCell(cell) else {
            return
        }
        
        self.saveValueForCellAtIndexPath(value, indexPath: indexPath)
        self.updateSaveButtonState()
    }
    
    func formCellDidTapReturn(cell: BookDetailFormCell) {
        guard let indexPath = self.tableView.indexPathForCell(cell) else {
            return
        }
        
        if indexPath.row == BookDetailFormField.numberOfValues() - 1 {
            self.view.endEditing(true)
        } else {
            guard let cell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: indexPath.row + 1, inSection: 0)) as? BookDetailFormCell else {
                return
            }
            
            cell.beginEditing()
        }
    }
    
    // MARK: BookDetailCheckoutViewDelegate
    
    func didTapCheckoutButtonForCheckoutView(checkoutView: BookDetailCheckoutView) {
        let checkoutNameManager = CheckoutNameManager()
        checkoutNameManager.delegate = self
        checkoutNameManager.askForName()
    }
    
    func didTapReturnButtonForCheckoutView(checkoutView: BookDetailCheckoutView) {
        guard var book = self.book else {
            return
        }
        
        book.lastCheckedOutBy = nil
        book.lastCheckedOut = nil
        self.updateBookForCheckoutAction(book)
    }

    func didUpdateConstraintsForCheckoutView(checkoutView: BookDetailCheckoutView) {
        self.tableView.tableFooterView = checkoutView
    }
    
    // MARK: CheckoutNameManagerDelegate
    
    func checkoutNameManagerDidRecieveName(manager: CheckoutNameManager, name: String) {
        guard var book = self.book else {
            return
        }

        book.lastCheckedOutBy = name
        book.lastCheckedOut = DateTime()
        self.updateBookForCheckoutAction(book)
    }
    
    func viewControllerForCheckoutNameManagerAlerts(manager: CheckoutNameManager) -> UIViewController {
        return self
    }
}
