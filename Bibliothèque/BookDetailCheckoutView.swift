//
//  BookDetailCheckoutView.swift
//  Bibliothèque
//
//  Created by Max Mamis on 11/2/15.
//  Copyright © 2015 Prolific Interactive. All rights reserved.
//

import UIKit

enum BookDetailCheckoutViewState {
    case IneligibleForCheckout
    case Available
    case CheckedOut(String)
}

protocol BookDetailCheckoutViewDelegate: class {
    func didTapCheckoutButtonForCheckoutView(checkoutView: BookDetailCheckoutView)
    func didTapReturnButtonForCheckoutView(checkoutView: BookDetailCheckoutView)
    func didUpdateConstraintsForCheckoutView(checkoutView: BookDetailCheckoutView)
}

class BookDetailCheckoutView: UIView {

    @IBOutlet private weak var actionButton: UIButton!
    @IBOutlet private weak var checkoutDescriptionLabel: UILabel!
    @IBOutlet private weak var checkoutDescriptionLabelHeightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var checkoutContainerView: UIView!
    
    weak var delegate: BookDetailCheckoutViewDelegate?
    
    private var state: BookDetailCheckoutViewState = .IneligibleForCheckout
    
    func setState(state:BookDetailCheckoutViewState, animated: Bool) {
        self.state = state
        self.updateForCurrentStateAnimated(animated)
    }
    
    @IBAction private func didTapActionButton() {
        switch self.state {
        case .Available:
            self.delegate?.didTapCheckoutButtonForCheckoutView(self)
            break
        case .CheckedOut(_):
            self.delegate?.didTapReturnButtonForCheckoutView(self)
            break
        default:
            break
        }
    }
    
    private func updateForCurrentStateAnimated(animated: Bool) {
        if !animated {
            self.performUpdatesForCurrentState()
        } else {
            UIView.animateWithDuration(0.2, delay: 0, options: .CurveEaseIn, animations: self.performUpdatesForCurrentState, completion: nil)
        }
    }
    
    private func performUpdatesForCurrentState() {
        switch self.state {
        case .IneligibleForCheckout:
            self.checkoutContainerView.alpha = 0
            break
        case .Available:
            self.checkoutContainerView.alpha = 1
            self.checkoutDescriptionLabelHeightConstraint.constant = 0
            self.actionButton.setTitle("Checkout", forState:.Normal)
            break
        case .CheckedOut (let checkoutDetails):
            self.checkoutContainerView.alpha = 1
            self.checkoutDescriptionLabel.text = checkoutDetails
            self.actionButton.setTitle("Return", forState: .Normal)
            let size = self.checkoutDescriptionLabel.sizeThatFits(CGSize(width: self.checkoutDescriptionLabel.frame.size.width, height:UIViewNoIntrinsicMetric))
            self.checkoutDescriptionLabelHeightConstraint.constant = size.height
            break
        }
        
        self.layoutIfNeeded()
        self.delegate?.didUpdateConstraintsForCheckoutView(self)
    }
    
}

