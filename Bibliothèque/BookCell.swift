//
//  BookCell.swift
//  Bibliothèque
//
//  Created by Max Mamis on 11/2/15.
//  Copyright © 2015 Prolific Interactive. All rights reserved.
//

import UIKit

class BookCell: UITableViewCell {
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var authorLabel: UILabel!

    func layoutWithBook(book: Book) {
        self.titleLabel.text = book.title
        self.authorLabel.text = book.author
    }
}
