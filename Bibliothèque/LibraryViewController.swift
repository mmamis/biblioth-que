//
//  ViewController.swift
//  Bibliothèque
//
//  Created by Max Mamis on 11/2/15.
//  Copyright © 2015 Prolific Interactive. All rights reserved.
//

import UIKit

class LibraryViewController: UITableViewController {
    private let cellIdentifier = "BookCell"
    
    private var books: [Book]?
        
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.setupDesign()
        self.loadData()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ShowBookDetail" {
            let detailVC = segue.destinationViewController as! BookDetailViewController
            if let row = self.tableView.indexPathForSelectedRow?.row,
                book = self.books?[row] {
                detailVC.book = book
            }
        }
    }

    // MARK: IBActions
    
    @IBAction private func deleteAll(sender: AnyObject) {
        let controller = UIAlertController.init(title: "Are you sure you want to do that?", message: "Deleting all books is unundoable.", preferredStyle: .ActionSheet)
        controller.addAction(UIAlertAction.init(title: "Whatever, I do what I want", style: .Destructive, handler: { (action) -> Void in
            self.deleteAllBooks()
        }))
        controller.addAction(UIAlertAction.init(title: "Cancel", style: .Cancel, handler: nil))
        self.presentViewController(controller, animated: true, completion: nil)
    }
    
    // MARK: Private Methods
    
    private func loadData() {
        LibraryFacade.sharedFacade.getAllBooks { (books, error) -> Void in
            if let error = error {
                self.showError(error)
            }
            
            if let books = books {
                self.books = books
                self.tableView.reloadData()
            }
        }
    }
    
    private func setupDesign () {
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 66
    }
    
    
    private func showError(error: ErrorType) {
        
    }
    
    private func deleteBookAtIndex(index: Int) {
        guard let book = self.books?[index] else {
            return
        }
        
        LibraryFacade.sharedFacade.deleteBook(book) { (error) -> Void in
            if let error = error {
                self.showError(error)
                return
            }
            
            self.books?.removeAtIndex(index)
            self.tableView.deleteRowsAtIndexPaths([NSIndexPath.init(forItem: index, inSection: 0)], withRowAnimation: .Automatic)
        }
    }
    
    private func deleteAllBooks() {
        LibraryFacade.sharedFacade.deleteAllBooks { (error) -> Void in
            if let error = error {
                self.showError(error)
            } else {
                self.books?.removeAll()
                self.tableView.reloadSections(NSIndexSet.init(index: 0), withRowAnimation: .Automatic)
            }
        }
    }

    // MARK: UITableViewDatasource / UITableViewDelegate
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = 0
        if let books = self.books {
            count = books.count
        }
        return count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("BookCell") as! BookCell

        if let book = self.books?[indexPath.row] {
            cell.layoutWithBook(book)
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            self.deleteBookAtIndex(indexPath.row)
        }
    }
    
}

