//
//  LibraryFacade.swift
//  Bibliothèque
//
//  Created by Max Mamis on 11/2/15.
//  Copyright © 2015 Prolific Interactive. All rights reserved.
//

import Foundation
import Alamofire
import Gloss

enum NetworkingError: ErrorType {
    case NoResponse
    case CouldNotDeserialize
    case InvalidModel
}

class LibraryFacade {
    
    static let sharedFacade = LibraryFacade()
    
    private let baseURL = "http://prolific-interview.herokuapp.com/55ba3cd4430a1d0009161085"
    
    func getAllBooks(completion:(books:[Book]?, error:ErrorType?) -> Void) {
        Alamofire.request(.GET, self.urlStringWithEndpoint("/books"), parameters: nil).responseJSON { response in
            
            if let responseError = response.result.error {
                completion(books: nil, error: responseError)
                return
            }
            
            var error: NetworkingError?
            var books: [Book]?
            
            if let json = response.result.value as? [JSON] {
                books = Book.modelsFromJSONArray(json)
                
                if books == nil {
                    error = .CouldNotDeserialize
                }
                
            } else {
                error = .NoResponse
            }
            
            completion(books: books, error: error)
        }
    }
    
    typealias SingleBookCompletion = (savedBook:Book?, error:ErrorType?) -> Void
    
    func postBook(book: Book, completion:SingleBookCompletion) {
        Alamofire.request(.POST, self.urlStringWithEndpoint("/books"), parameters: book.toJSON(), encoding: .JSON, headers: nil).responseJSON { response in
            self.dealWithBookResponse(response, completion: completion)
        }
    }
    
    func updateBook(book: Book, completion:SingleBookCompletion) {
        guard let url = book.url else {
            completion(savedBook: nil, error: NetworkingError.InvalidModel)
            return
        }
        
        Alamofire.request(.PUT, self.urlStringWithEndpoint(url), parameters: book.toJSON(), encoding: .JSON, headers: nil).responseJSON { response in
            self.dealWithBookResponse(response, completion: completion)
        }
    }
    
    func dealWithBookResponse(response: Alamofire.Response<AnyObject, NSError>, completion: SingleBookCompletion) {
        
        if let responseError = response.result.error {
            completion(savedBook: nil, error: responseError)
            return
        }
        
        var error: NetworkingError?
        var book: Book?
        
        if let json = response.result.value as? JSON {
            book = Book.init(json: json)
            
            if book == nil {
                error = .CouldNotDeserialize
            }
        } else {
            error = .NoResponse
        }
        
        completion(savedBook: book, error: error)
    }
    
    func deleteBook(book: Book, completion: (error: ErrorType?) -> Void) {
        guard let url = book.url else {
            completion(error: NetworkingError.InvalidModel)
            return
        }

        Alamofire.request(.DELETE, self.urlStringWithEndpoint(url)).response { (request, response, data, error) -> Void in
            completion(error: error)
        }
    }
    
    func deleteAllBooks(completion: (error: ErrorType?) -> Void) {
        Alamofire.request(.DELETE, self.urlStringWithEndpoint("/clean")).response { (request, response, data, error) -> Void in
            completion(error: error)
        }
    }
    
    private func urlStringWithEndpoint(endpoint: String) -> String {
        return "\(self.baseURL)\(endpoint)"
    }
    
}