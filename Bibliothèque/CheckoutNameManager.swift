//
//  CheckoutNameManager.swift
//  Bibliothèque
//
//  Created by Max Mamis on 11/2/15.
//  Copyright © 2015 Prolific Interactive. All rights reserved.
//

import UIKit

protocol CheckoutNameManagerDelegate: class {
    func viewControllerForCheckoutNameManagerAlerts(manager: CheckoutNameManager) -> UIViewController
    func checkoutNameManagerDidRecieveName(manager: CheckoutNameManager, name: String)
}

class CheckoutNameManager: NSObject {
    weak var delegate : CheckoutNameManagerDelegate?
    
    private let userDefaultsKey = "CheckoutName"
    
    func askForName() {
        if let name = self.nameFromUserDefaults() {
            self.promptToUseExistingName(name)
        } else {
            self.promptToEnterNewName()
        }
    }
    
    private func promptToUseExistingName(name: String) {
        let controller = UIAlertController.init(title: "What name would you like you use?", message: "You've already checked out with the name \(name).", preferredStyle: .ActionSheet)
        controller.addAction(UIAlertAction.init(title: "Checkout as \(name)", style: .Default, handler: { (action) -> Void in
            self.updateDelegateWithName(name)
        }))
        controller.addAction(UIAlertAction.init(title: "Enter new name", style: .Default, handler: { (action) -> Void in
            self.promptToEnterNewName()
        }))
        controller.addAction(UIAlertAction.init(title: "Cancel", style: .Cancel, handler: nil))
    
        viewControllerForAlerts()?.presentViewController(controller, animated: true, completion: nil)
    }
    
    private func promptToEnterNewName() {
        let controller = UIAlertController.init(title: "Please enter your name", message: nil, preferredStyle: .Alert)
        controller.addAction(UIAlertAction.init(title: "OK", style: .Default, handler: { (action) -> Void in
            if let text = controller.textFields?.first?.text {
                self.saveName(text)
                self.updateDelegateWithName(text)
            }
        }))
        controller.addAction(UIAlertAction.init(title: "Cancel", style: .Cancel, handler: nil))
        controller.addTextFieldWithConfigurationHandler { (textField) -> Void in
            textField.placeholder = "Name, please..."
        }

        viewControllerForAlerts()?.presentViewController(controller, animated: true, completion: nil)
    }
    
    private func viewControllerForAlerts() -> UIViewController? {
        return self.delegate?.viewControllerForCheckoutNameManagerAlerts(self)
    }
    
    private func updateDelegateWithName(name: String) {
        self.delegate?.checkoutNameManagerDidRecieveName(self, name: name)
    }
    
    private func saveName(name: String) {
        NSUserDefaults.standardUserDefaults().setValue(name, forKey: self.userDefaultsKey)
    }
    
    private func nameFromUserDefaults() -> String? {
        return NSUserDefaults.standardUserDefaults().stringForKey(self.userDefaultsKey)
    }
}
